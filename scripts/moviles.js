var bootstrap = function() {
    var url = Config.url;
	var urlTrucks = '/supporttrucks/';
    var urlStates = '/truckstates/';
    var urlPositions = '/positions/'
	
    var map = createMap('mapaMoviles');
    var drawer = new Drawer();

    var movilIcon = L.Icon.extend({
        options: {
            iconSize:     [36, 33],
            shadowSize:   [50, 64],
            iconAnchor:   [12, 14],
            shadowAnchor: [4, 62],
            popupAnchor:  [-3, -76]
        }
      });

      var resolverIcono = function(state){
        if(state == 0){
          return new movilIcon({iconUrl: 'leaflet/images/movil.png'});
        }
        else if(state == 1){
          return new movilIcon({iconUrl: 'leaflet/images/movil.png'});
        }
        else{
          return new movilIcon({iconUrl: 'leaflet/images/movil.png'});
        } 
      }
    

    var requestSupportsTruck = function (){
        return $.ajax(url + urlTrucks)
    }
    var requestSupportTruck = function(supportTruck_id) {
        return $.ajax(url + urlTrucks + supportTruck_id);
    }
    var requestPositions = function(supportTruck){
        return $.ajax(url +  urlTrucks + supportTruck.id  + urlPositions)
    }
    
    var requestStates = function() {
        return $.ajax(url + urlStates);
    }
    var responseExtract = function(attr, response) {
        return response[attr]
    }
    var extractSupportTruck = function(response) {
        return responseExtract('supportTruck', response);
    }

    var drawSupportTruck = function(supportTruck) {
        drawer.drawSupportTruckInMap(supportTruck, map);
    }

    var drawStatesInList = function(supportTruckId, description) {
        drawer.drawStatesInList(supportTruckId, description);
    }

    
    var updater = function(indx, supportTruck){
        setTimeout(function() {
            //Informacion actual: Estado y Posicion
            supportTruck.infoActual = supportTruck.positions[indx]
            //console.log(supportTruck.infoActual.state)
            var icono = resolverIcono(supportTruck.infoActual.state)
            supportTruck.icono = icono
            
            drawSupportTruck(supportTruck)
            
            drawStatesInList(supportTruck.id, supportTruck.infoActual.description)
            
            actualIx = indx + 1
            if(actualIx < supportTruck.positions.length) {
                updater( actualIx, supportTruck)
            }
        }, 4000)
    }

    var resolvePositionSupportTruck = function(supportTruck){
        return requestPositions(supportTruck)
               .then(function(response){
                   supportTruck.positions = response.positions
                   delete supportTruck.position
                   delete supportTruck.state_id
                   return supportTruck
               });
    }
   
    var resolveStatesSupportTruck = function(supportTruck) {
        return requestStates()
                .then(response => {
                    supportTruck.positions.map(position => position.description = response.states[position.state].description)
                    return supportTruck
                })
 
    }

    var mostrarInformacion = function(supportTruck) {
        var movilLayer = L.layerGroup().addTo(map);
        map.layersControl.addOverlay(movilLayer, supportTruck);
        //Le agrego las cosas que quiero mostrar al Movil
        supportTruck.movilLayer = movilLayer
        supportTruck.icono = ""
        console.log(supportTruck)

        updater(0, supportTruck)
    }
   
    requestSupportsTruck()
    .then(response => {
        console.log(response.supportTrucks)
        response.supportTrucks.forEach(supportTruck => {
        requestSupportTruck(supportTruck.id) 			
            .then(extractSupportTruck) 		
            .then(resolvePositionSupportTruck) 
            .then(resolveStatesSupportTruck)
            .then(mostrarInformacion)
            .done(function() {
                console.log("Fin.");
            });
        })
    })        
};

$(bootstrap);
