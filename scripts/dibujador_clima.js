var nodoPadre = null;
var urlPronosticos = "https://ws.smn.gob.ar/map_items/weather";
var localidad = "San Miguel";

var aux = obtenerJsonDe(urlPronosticos);      
;    aux.done( function () {
        dibujarWidgetClima(aux);
    });


var dibujarWidgetClima = function (respuesta) {
        pronosticos = respuesta.responseJSON;                                                                               //filtra la respuesta del JSON
        var pronosticoDe = obtenerLocalidad(localidad, pronosticos);                                                        //busca de la respuesta de JSON la localidad dada
    
        var nodoPadre = obtenerNodoConId("clima actual");                                                                   //se obtiene el nodo al que se le agregaran los elementos que componen el widget
        var nodoLocalidad = crearElementoTipoConTexto("p", "Localidad: "+pronosticoDe.name+", "+pronosticoDe.province);     //se crea un elemento con el nombre de la localidad y la provincia
      
        var climaEnLocalidad = pronosticoDe.weather;                                                                        //se filtra de los datos los datos del tipo clima
        var nodoDescripcion = crearElementoTipoConTexto("p", climaEnLocalidad.description);                                 //se crea um elemento con la descripcion del clima
  
        var nodoTemperatura = crearElementoTipoConTexto("p", climaEnLocalidad.tempDesc);                                    //se crea un elemento con la temperatura actual
      
        var nodoHumedad = crearElementoTipoConTexto("p", "Humedad: "+climaEnLocalidad.humidity+"%");                        //se crea un elemento con la humedad actual
  
        var nodoViento = crearElementoTipoConTexto("p", "Viento direccion "+climaEnLocalidad.wing_deg+" a "+climaEnLocalidad.wind_speed+" Km/h");   //elemento con el viento y direccion
       
        agregarElementosAlPadre(nodoPadre, [nodoLocalidad,              //se agrega todos los elementos en un orden especifico al
                                            nodoDescripcion,                //elemento que aloja este nuevo elemento
                                            nodoTemperatura,
                                            nodoHumedad,
                                            nodoViento]);
}

var obtenerLocalidad = function (nombre, json) {                                    //con el nombre y la respuesta del JSON se consigue el pronostico de la localidad nombre
        var respuesta = null;
        json.forEach( element => {                                                  //por cada elemento si es lo localidad buscada se guarda sus atributos
            respuesta = element.name == nombre ?  element : respuesta;
        });
        return respuesta;
    }

