var bootstrap = function() {
    var map = createMap('mapaMoviles');
    var drawer = new Drawer();
    var centroIcon = L.Icon.extend({
        options: {
            iconSize:     [36, 33],
            shadowSize:   [50, 64],
            iconAnchor:   [12, 14],
            shadowAnchor: [4, 62],
            popupAnchor:  [-3, -76]
        }
      });
     
   fetch('centros/centros.txt')
  .then(res => res.text())
  .then(content => JSON.parse(content))
  .then(object => { 
        console.log(object.centros)
        object.centros.forEach( centro =>
        drawer.drawCenterInMap(centro, map, new centroIcon({iconUrl: 'leaflet/images/centro.png'}))
  )})
    
}
$(bootstrap);
