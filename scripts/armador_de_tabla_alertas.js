var alertasTotales = obtenerJsonDe(servicioDeAlertasSMN);                       //obtiene archivo JSON de la url
        alertasTotales.done(function() {                                        //en cuanto termina ejecuta la funcion
            var alertas = alertasTotales.responseJSON;                          //de la informacion traida en el JSON, se queda con la respuesta que sirve
            var nodoPadre = obtenerNodoConId("cuadro clima");                   //obtiene el nodo donde se creara las alertas

            crearCadaAlertaDe(alertas, nodoPadre);                              //toma la lista de alertas y crea cada una en el nodoPadre
 
        });

var crearCadaAlertaDe = function (alertas, nodoPadre) {                         
            alertas.forEach(alerta => {                                         
                var nodoHijo = crearElementoTipo("tr");                         //crea la casilla donde se pondra la informacion de un alerta particular

                var nodoTipo = crearElementoTipo("h3");                         //crea un elemento con el tipo de alerta
                agregarTextoAlElemento(nodoTipo,"Tipo de Alerta: " + alerta.title);

                var nodoUbicacion = crearElementoTipo("p");                     //crea un elemento p donde se pondra cada ubicacion afectada
                agregarTextoAlElemento(nodoUbicacion, "Ubicacion: ");               //por la alerta
                agregarZonasEnAlerta(alerta.zones, nodoUbicacion);              //recorre la lista de zonas y las agrega al nodo de ubicaciones

                var nodoHora = crearElementoTipo("p");                          //crea un elemento p donde se pondra la hora de la alerta
                agregarTextoAlElemento(nodoHora, "Hora: " + alerta.hour);

                var nodoAlerta = crearElementoTipo("p");                        //crea un elemento p donde luego se pone la descripcion de la alerta
                agregarTextoAlElemento(nodoAlerta,"Alerta: " + alerta.description);

                agregarElementosAlPadre(nodoHijo, [nodoTipo,                    //coloca toda la informacion recolectada por partes en un nodo tipo casilla
                                                    nodoUbicacion,
                                                    nodoHora,
                                                    nodoAlerta]);

                agregarElementosAlPadre(nodoPadre, [nodoHijo]);                 //coloca el nodo creado con la informacion dentro del cuadro
                });
        }

 var agregarZonasEnAlerta = function (zonas, nodoUbicacion) {
            var i = 0;                                                          //inicializa el for en 0, ya que el JSON devuelve un objeto con zonas
                                                                                    //que no esta ordenado en una lista, y tampoco tiene longitud
            while (zonas[i] != undefined) {                                     //mientras el elemento sea una zona
                nodoUbicacion.textContent = nodoUbicacion.textContent + zonas[i];   //se agrega la zona a la casilla
                if (zonas[i+1] == undefined) {                                      //si no existe proxima zona, se agrega un punto al texto de zonas
                    nodoUbicacion.textContent = nodoUbicacion.textContent + ".";
                }
                else{                                                               //si existe proximas zonas, se agrega una coma para seguir luego agregando.
                    nodoUbicacion.textContent = nodoUbicacion.textContent + ", ";
                }                
                i = i + 1;                                                          //se avanza a la proxima zona de la lista.
            }
        }