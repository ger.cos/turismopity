var Drawer = function() {
    return {
        drawSupportTruckInMap: drawSupportTruckInMap,
        drawStatesInList: drawStatesInList,
        drawCenterInMap: drawCenterInMap
    }

    function drawCenterInMap(center, map, icono){
        console.log('grax x venir')
        var centroLayer = L.layerGroup().addTo(map);
        map.layersControl.addOverlay(centroLayer, center);
        centroLayer.addLayer(L.marker(L.latLng(center.informacion.position.lat, center.informacion.position.lon), {icon: icono}).bindPopup(center.informacion.descripcion).addTo(map))
    }

    function drawSupportTruckInMap(supportTruck, map) {

        supportTruck.movilLayer.clearLayers()
        supportTruck.movilLayer.addLayer(L.marker(L.latLng(supportTruck.infoActual.position.lat,supportTruck.infoActual.position.lon), {icon: supportTruck.icono}).bindPopup("ID: s" + supportTruck.id + "\n" +"("+ supportTruck.infoActual.description+")").addTo(map))
    }
	
    function drawStatesInList(supportTruckId, description) {        
		//states.forEach(function(state) {
            
            var estadosDeLosMoviles = document.getElementById('estadosDeLosMoviles');
            //var estadoActual = document.createElement('li');
            //estadosDeLasGruas.appendChild(estadoActual);

            estadoActual = document.createElement('li');
            estadoActual.accessKey = supportTruckId
            estadoActual.appendChild(document.createTextNode("Movil " + supportTruckId+ ": "));
            estadoActual.appendChild(document.createTextNode(description));
            estadoActual.appendChild(document.createElement('br'));
            estadoActual.appendChild(document.createElement('br'));
            //onsole.log(estadosDeLosMoviles.getChilds())
            if (Array.from(estadosDeLosMoviles.childNodes).filter(node => node.accessKey == supportTruckId)[0]
            != undefined){
                estadosDeLosMoviles.replaceChild(estadoActual, 
                    Array.from(estadosDeLosMoviles.childNodes).filter(node => node.accessKey == supportTruckId)[0])    
            }
            else{
                estadosDeLosMoviles.appendChild(estadoActual)
            }
            
            //console.log('Estado Actual:' + estadoActual)
            //console.log('Valor a reemplazar:' +  Array.from(estadosDeLosMoviles.childNodes).filter(node => node.accessKey == supportTruck.id)[0] )


            //var  l = [1,3,5,6]
           //l.filter(n => n %2 == 0 ).forEach( e => console.log(e)) 
    }
}
