var obtenerNodoConId = function (nombreId) {                //obtiene elemento a travez del id
    var nodo = document.getElementById(nombreId);
    return nodo;
}

var obtenerJsonDe = function (url) {                        //obtiene documento JSON desde la url dada
    var pedido = $.getJSON(url);
    return pedido;
}

var crearElementoTipo = function (tipo) {                   //crea un elemento del tipo dado
    return nodo = document.createElement(tipo);
}

var agregarTextoAlElemento = function (elemento, texto) {   //agrega texto a un elemento dado
    elemento.textContent = texto;
}

var crearElementoTipoConTexto = function (tipo, texto) {    //crea un elemento de un tipo dado con un texto dado
    var nodo = crearElementoTipo(tipo);
    agregarTextoAlElemento(nodo, texto);
    return nodo;
}

var agregarElementosAlPadre = function(nodoPadre, nodosHijos) {     //agrega elementos a un nodo a partir de un array.
    nodosHijos.forEach( nodo => {
        nodoPadre.appendChild(nodo);        
    });
}

var crearBotonRadio = function (nombre, valor, texto) {             
    var nodo = crearElementoTipo("input");
    nodo.type = "radio";
    nodo.name = nombre;
    nodo.value = valor;
    nodo.onclick = function() {"selectorDeDia("+i+");";}
    return nodo;
}

var crearBotonConAccion = function (valor, nombre) {                //crea un boton que al dar click ejecuta una funcion que reconoce el
    var nodo = crearElementoTipo("button");                             //dia que se esta pidiendo informacion
    nodo.type = "button";
    nodo.value = valor;
    nodo.name = nombre;
    nodo.innerText = nombre;
    nodo.onclick = function () { selectorDeDia(valor); };
    return nodo;
}

var selectorDeDia = function (numero) {                             //añade el dia a la url si es para el servicio de alertas assistence,
    if (urlServicio == servicioDeAlertasAssistance) {                   //sino, la url queda igual
        urlPedido = urlServicio + numero;
    }
    else if (urlServicio == servicioDeAlertasSMN) {
        urlPedido = urlServicio;
    }
    pedirAlertas(urlPedido);                                        //ejecuta el proximo paso del dibujado
}

var pedirAlertas = function (url, nodoDondeDibujar) {               //pide un JSON con las alertas de la url configurada
    var nodo = nodoDondeDibujar;
    var pedidoDeAlertas = $.getJSON(url);
    pedidoDeAlertas.done( function () {
        dibujarAlertas(pedidoDeAlertas.responseJSON, nodo);         //empieza el proceso de dibujado
    })
}

var dibujarAlertas = function (respuesta, nodoDondeDibujar) {       
    var alertas;
    if (urlServicio == servicioDeAlertasAssistance) {               
        alertas = respuesta.alerts;                                 //si la respuesta es de alerta assistence, recupera la respuesta del JSON
    }
    else if (urlServicio == servicioDeAlertasSMN) {
        alertas = respuesta;                                        //si la respuesta es de la SMN la respuesta ya es el responseJSON
    }
    console.log(alertas);
    var nodo = nodoDondeDibujar;
    nodo = document.getElementById("infoAlertas");                  //identifica donde dibujar las alertas
    crearCadaAlertaDe(alertas, nodo);                               //por cada alerta de la lista de alertas la imprime en el nodo
}

var crearCadaAlertaDe = function (alertas, nodoPadre) {
    $(nodoPadre).empty();
    alertas.forEach(alerta => {
        var nodoHijo = crearElementoTipo("tr");

        var nodoTipo = crearElementoTipo("h3");
        agregarTextoAlElemento(nodoTipo,"Tipo de Alerta: " + alerta.title);

        var nodoUbicacion = crearElementoTipo("p");
        agregarTextoAlElemento(nodoUbicacion, "Ubicacion: ");
        agregarZonasEnAlerta(alerta.zones, nodoUbicacion);

        var nodoHora = crearElementoTipo("p");
        agregarTextoAlElemento(nodoHora, "Hora: " + alerta.hour);

        var nodoAlerta = crearElementoTipo("p");
        agregarTextoAlElemento(nodoAlerta,"Alerta: " + alerta.description);

        agregarElementosAlPadre(nodoHijo, [nodoTipo,                //agrega los elementos al nodo que se incluira luego en el documento
                                            nodoUbicacion,
                                            nodoHora,
                                            nodoAlerta]);

        agregarElementosAlPadre(nodoPadre, [nodoHijo]);             //agrega el nodo de la alerta al documento en el nodoPadre
        });
}

var agregarZonasEnAlerta = function (zonas, nodoUbicacion) {        //agrega las zonas afectadas por la alerta
    var i = 0;

    while (zonas[i] != undefined) {
        nodoUbicacion.textContent = nodoUbicacion.textContent + zonas[i];
        if (zonas[i+1] == undefined) {
            nodoUbicacion.textContent = nodoUbicacion.textContent + ".";
        }
        else{
            nodoUbicacion.textContent = nodoUbicacion.textContent + ", ";
        }                
        i = i + 1;            
    }
}