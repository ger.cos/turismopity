var urlServicio;
var urlPedido;
var infoAlertas = obtenerNodoConId("infoAlertas");                      //obtiene el nodo para dibujar la informacion                     

var agregarBotones = function (valor, cantidad) {                       //reconoce las csillas donde se pondra la informacion
    var nodoPadre = obtenerNodoConId("cuadroDias");                         
    var nodoHijo = obtenerNodoConId("mostrarDias");
    seleccion(valor);                                                   //filtra el servidor que se va a usar
    limpiarForm(nodoPadre, nodoHijo);                                   //limpia la casilla por si se selecciona otro servidor
    cargarDias(cantidad);                                               //carga botones e informacion cuadno se los presionan
} 

var seleccion = function (value) {
    if (value == "smn") {                                               //si se selecciono el radio de servicio meteorologico nacional se configura aca
        urlServicio = servicioDeAlertasSMN;
    }
    else if (value == "as") {                                           //si se selecciono el radio de alertas service se configura aca
        urlServicio = servicioDeAlertasAssistance;
    }
    console.log("se ha seleccionado el servicio: " + urlServicio);
}

var cargarDias = function (cantidad) {                                   
    var nodoPadre = obtenerNodoConId("mostrarDias");                    //se consigue donde poner los nuevos elementos 
    for (let i = 0; i < cantidad; i++) {
        var nodoDia = crearBotonConAccion(i, "Dia "+ (i+1));            //se crean botones por la cantidad de dias de alerta que haya
        nodoPadre.appendChild(nodoDia);
        nodoPadre.appendChild(crearElementoTipo("br"));
    console.log(nodoPadre);
    }
}

var limpiarForm = function (padre, hijo) {                              
    var nuevoForm = crearElementoTipo("form");                          //se crea un form nuevo
    nuevoForm.id = hijo.id;                                             //se configura el id del nuevo form para no romper codigo dependiente
    hijo.remove();                                                      //se quita el form viejo
    padre.appendChild(nuevoForm);                                       //se agrega el form nuevo
    
}
